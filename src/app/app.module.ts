import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
//______________
import { HttpModule } from '@angular/http';
import {   HttpClientModule } from '@angular/common/http';
import {AngularFireModule} from "angularfire2";

//_________________

// Routes
import { APP_ROUTING } from './app.routes';

// Service
import { HeroesService } from './services/heroes.service';

// Components
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { HomeComponent } from './components/home/home.component';
import { AboutComponent } from './components/about/about.component';
import { HeroesComponent } from './components/heroes/heroes.component';
import { HeroeComponent } from './components/heroe/heroe.component';
import { HeroeSearchComponent } from './components/heroe-search/heroe-search.component';
import { HeroeTarjetaComponent } from './components/heroe-tarjeta/heroe-tarjeta.component';

export const firebaseConfig = {
  apiKey: "AIzaSyCH0TDQLHgqkmvWdpaFi64AWxFWWRyrNOE",
  authDomain: "spaheroes-a0400.firebaseapp.com",
  databaseURL: "https://spaheroes-a0400.firebaseio.com",
  storageBucket: "spaheroes-a0400.appspot.com",
  messagingSenderId: "765149670625",
  appId: "1:765149670625:web:cbda9df36b941476"
};

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    AboutComponent,
    HeroesComponent,
    HeroeComponent,
    HeroeSearchComponent,
    HeroeTarjetaComponent
    
  ],
  imports: [
    BrowserModule,
    APP_ROUTING,
   //_________________
    HttpModule,
    HttpClientModule,
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  providers: [
    HeroesService,
    
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
